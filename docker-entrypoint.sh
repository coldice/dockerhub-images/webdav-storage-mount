#!/usr/bin/env sh

set -e

log() {
    echo "$1: $2"
}

# Check if variables are set.
[ -z "$DAV_URL" ] && { log "ERROR" "URL has not been set"; exit 1; }
[ -z "$DAV_USERNAME" ] && log "INFO" "USERNAME has not been set";
[ "$DAV_PASSWORD_FILE" ] && { DAV_PASSWORD=$(head -n1 $DAV_PASSWORD_FILE); log "INFO" "Password file has been read"; }
[ -z "$DAV_PASSWORD" ] && log "INFO" "PASSWORD has not been set";

# Write credentials to config
echo "\"${DAV_URL//\"/\\\"}\" \"${DAV_USERNAME//\"/\\\"}\" \"${DAV_PASSWORD//\"/\\\"}\"" >> /etc/davfs2/secrets
unset DAV_PASSWORD

# Use command arguments as direct config parameters for davfs2.
# There is no validation on config parameters.
for i; do
    log "INFO" "setting config parameter $i"
    echo $i >> /etc/davfs2/davfs2.conf
done

# Create user for mount and set permissions.
adduser webdav -DHS -u $DAV_UID 2> /dev/null || true
addgroup webdav -S -g $DAV_GID 2> /dev/null || true
chown -R $DAV_UID:$DAV_GID /mnt

# Mount volume.
log "INFO" "mounting $DAV_URL with UID:GID $DAV_UID:$DAV_GID on /mnt..."
mount.davfs $DAV_URL /mnt -o uid=$DAV_UID,gid=$DAV_GID,dir_mode=755,file_mode=755

# Check if the mount was successful (at least lost+found is present)
[ "$(ls -A /mnt)" ] && log "INFO" "volume has been mounted" || { log "ERROR" "failed to mount volume"; exit 1; }

# Trap for catching signal to exit.
unmount() {
    SIGNAL=$1
    log "INFO" "received signal $SIGNAL, try unmounting"
    umount -l /mnt

    # Kill davfs2 if it didn't exit on a busy mount (.pid file must be cleaned up)
    log "INFO" "forwarding signal $SIGNAL to davfs2..."
    while $(pkill -$SIGNAL mount.davfs 2> /dev/null); do
        sleep 1
    done
    log "INFO" "davfs2 stopped"

    # Clear trap and exit
    exit 0
}

trap "unmount INT" INT
trap "unmount TERM" TERM

# Sleep "forever"
while :; do sleep 2073600; done
