FROM alpine:3.11.5

# Credentials for WebDAV share. If using PASSWORD_FILE, PASSWORD is ignored.
ENV DAV_URL=
ENV DAV_USERNAME=
ENV DAV_PASSWORD=
ENV DAV_PASSWORD_FILE=

# Files will be mounted using this UID and GID.
ENV DAV_UID=0
ENV DAV_GID=0

RUN apk add --no-cache tini ca-certificates davfs2=1.5.5-r0

COPY docker-entrypoint.sh /

HEALTHCHECK --interval=10s --timeout=10s CMD [ '[ -n "$(ls -A /mnt)" ]' ]

ENTRYPOINT [ "tini", "-g", "--", "/docker-entrypoint.sh" ]
