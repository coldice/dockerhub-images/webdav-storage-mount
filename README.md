[Git Repository](https://gitlab.com/coldice/dockerhub-images/webdav-storage-mount)

# WebDAV Storage Mount for Docker
This Docker image allows to mount WebDAV volumes on shared bind mounts and provide those to other containers. It is based on Alpine Linux using [davfs2](https://savannah.nongnu.org/projects/davfs2) as WebDAV client. All config parameters for davfs2 can be set by using command line parameters in Docker. The tag versions are based on the davfs2 version used inside the container.

## Configuration
The following environment variables can be used:
- `DAV_URL` points to the remote WebDAV resource using `http://` or `https://` schema (required, default `""`)
- `DAV_USERNAME` is the username for the WevDAV resource (optional, default `""`)
- `DAV_PASSWORD` is the password for the WevDAV resource (optional, default `""`)
- `DAV_PASSWORD_FILE` is a path to a file inside the container which contains the password (optional, overrides `DAV_PASSWORD`, default `""`)
- `DAV_UID` is the system user ID used to mount the ressource (optional, default `0`)
- `DAV_GID` is the system group ID used to mount the ressource (optional, default `0`)

When using `DAV_PASSWORD_FILE` only the first line of the file is considered the secret. To set custom config parameters for davfs2, each parameter must be provided as a command line argument to the Docker entry point - see examples.

## Example (docker-compose)
```yaml
version: "3"
services:
  storage:
    image: cldc/webdav-storage-mount:latest
    restart: unless-stopped
    command:
      - "use_locks 1"
      - "lock_timeout 1800"
      - "lock_refresh 60"
    environment:
      DAV_URL: https://my-webdav.endpoint.tld
      DAV_USERNAME: user1
      DAV_PASSWORD: secret-pa55word
      DAV_UID: 101
      DAV_GID: 101
    devices:
      - /dev/fuse
    cap_add:
      - SYS_ADMIN
    security_opt:
      - apparmor=unconfined
    volumes:
      - /mnt/app/html:/mnt:rshared

  app:
    image: nginx
    restart: unless-stopped
    depends_on:
      - storage
    ports:
      - 8080:80
    volumes:
      - /mnt/app:/usr/share/nginx:rslave
```
You can omit all command line arguments to use the default settings (see [here](https://linux.die.net/man/5/davfs2.conf)).

In order to create the fuse mount `/dev/fuse` must be available for the container and it requires the `SYS_ADMIN` capability.

To propagate the mount from the container to the system and other containers `rshared` and `rslave` are essential volume options.

## Quirks
Do not mount the WebDAV mounted folder or subfolders of it directly into other containers as bind mounts. Always mount a parent directory containing the mount into other containers. Otherwise Docker will not able to recreate the mount on reconnect or recreation of this container without restarting the dependent ones -> If you have bind mounted your WebDAV volume on `/mnt/app/html` do not mount `/mnt/app/html` directly in other containers, use `/mnt/app` instead.